/**
 * @author David 'dubak' Dubovsky
 * @link http://www.dubak.sk/tools/online-captcha-pictures-generator
 * @project https://bitbucket.org/dubak/captcha/
 */

$( function()
{	
	$('i[data-id="reload"]').on('click', function()
	{
		console.log('clicked');		
		$('iframe[data-id="captcha-embed"]').attr( 'src', function ( i, val ) { return val; });
	});
});