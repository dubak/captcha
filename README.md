# About project #

PHP class generating CAPTCHA pictures

### How do I get set up? ###

* Download / clone into your local server
* Visit landing page from your web browser
* Dependencies (PHP 5.6 > )

### Contribution guidelines ###

* Every code extending functionality is welcomed and appreciated
* Clone the project, and your code into new branch and send back as merge request

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### Summary ###

* Version 0.1
* [Project founder link](http://www.dubak.sk)