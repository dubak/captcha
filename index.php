<?
session_name('captcha');
session_start('captcha');
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>PHP class generating CAPTCHA pictures | Created by dubak.sk</title>

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta http-equiv="content-language" content="en" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2" />
		<meta name="keywords" content="programming in PHP, LAMP technology fan and professional, databases, web marketing, SEO" />
		<meta name="description" content="dubak, online specialist, blog - PHP class generating CAPTCHA pictures" />	
		<meta name="robots" content="index,follow" />
		<meta name="allow-search" content="yes" />
		<meta name="distribution" content="Global" />
		<meta http-equiv="pragma" content="no-cache" />
		<meta name="author" content="David Dubovsky http://www.dubak.sk" />

		<link rel="shortcut icon" href="./assets/img/captcha-favicon.ico" type="image/x-icon" />
		<link rel="icon" href="./assets/img/captcha-favicon.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" href="./assets/img/captcha-favicon.ico" />

		<!-- CSS -->
		<link rel="stylesheet" href="./assets/css/bootstrap/3.3.5/captcha-theme.css" type="text/css"  />		
		<link rel="stylesheet" href="./assets/css/captcha-main.css" type="text/css" />

		<!-- JavaScript -->
		<script type="text/javascript" src="./assets/js/jquery-1.11.min.js"></script>
		<script type="text/javascript" src="./assets/js/bootstrap-3.3.5.min.js"></script>
		<script type="text/javascript" src="./assets/js/captcha-main.js"></script>

	</head>
	<body class="captcha">					
		
		<div class="container-lg">
			<div class="captcha-grid">
				<div class="grid-holder">
					<div id="my-tab-content" class="tab-content">
						<div class="tab-pane active">
							<? 
							if ( isset($_POST['code']) && !empty($_POST['code']) ) {
								if ( !empty($_SESSION['captcha']['code']) && strtolower(trim($_SESSION['captcha']['code'])) == strtolower(trim($_POST['code'])) ){
									$succes = true;
								}
								else {
									$succes = false;
								}
							}
							?>
							<form action="./" method="post" class="captcha-form">																
								<? if ( isset($succes) ) : ?>								
									<? if ( $succes ) : ?>
										<div class="alert alert-success" role="alert"><strong>Well done!</strong> You successfully verified the captcha :-)</div>
									<? else : ?>
										<div class="alert alert-danger" role="alert"><strong>Oh snap!</strong> Incorrect verification :-(</div>
									<? endif; ?>	
								<? endif; ?>
								
								<div class="form-group captcha-embed">
									<iframe type="image/png" src="./generate-captcha" data-id="captcha-embed" scrolling="no" marginheight="0px" marginwidth="0px" frameborder="0" height="50px" width="160px"></iframe>      
									<i class="glyphicon glyphicon-refresh fa-2x" data-id="reload" title="Generate new picture"></i>
								</div>								
								
								<div class="form-group captcha-letters">
									<label for="form-letters" class="control-label">Captcha letters</label>
									<input type="text" name="code" id="form-letters" class="form-control input-lg" placeholder="Write letters from picture" required="true" />
								</div>

								<button type="submit" class="btn btn-success btn-lg btn-block">
									<span class="glyphicon"></span> Verify
								</button>
							</form>

							<div>
								<p class="text-center">
									Link to author's <a href="http://www.dubak.sk/blog">blog.</a>
								</p>
								<p class="text-center">
									Link to  <a href="https://bitbucket.org/dubak/captcha/">code source</a> on Bitbucket.
								</p>								
							</div>
						</div>					
					</div>
				</div>
			</div>
		</div>
				
	  <div class="tiny-clear"></div>
	</body>
</html>
