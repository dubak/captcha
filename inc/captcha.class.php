<?php

namespace captcha;

/**
 * Class for generating CAPTCHA
 * 
 * @author David 'dubak' Dubovsky <http://www.dubak.sk/blog>
 * @see https://bitbucket.org/dubak/captcha/
 */
final class CaptchaPicture 
{
	/**
	 * @var string Font
	 * @access private
	 * @static
	 */
	private static $font = './font.ttf';

	/**
	 * @var array Character angels
	 * @access private
	 * @static
	 */
	private static $charsAngels = array(10, 30, 0, 50, -20, 0);

	/**
	 * @var array Default picture values
	 * @access private
	 * @static
	 */	
	private static $defaults = array (
		'pic_width' => 120,
		'pic_height' => 40,
		'chars_count' => 5
	);
	
	/**
	 * @var array Code letters
	 * @access private
	 * @static
	 */
	private static $code = array();

	
	/**
	 * @param void
	 * @return void
	 * @access private
	 * @throws Exception
	 */
	private function __construct ( )
	{
		throw new Exception('Class is static.', E_USER_ERROR);
	}
	
	/**
	 * Check picture width
	 * 
	 * @param integer $width Picture width
	 * @return integer Verified picture width
 	 * @access private
	 * @static
	 */
	private static function checkWidth ( $width )
	{
		$resultWidth = self::$defaults['pic_width'];
		
		$width = intval($width);
		
		if ( !empty($width) && $width > 100 && $width < 300 ) {
			$resultWidth = $width; 	
		}
		
		return $resultWidth;
	}
	
	
	/**
	 * Render captcha picture
	 * 
	 * @param integer $name Description
	 * @return image/png
	 * @access public
	 * @static
	 */ 
	public static function render ( $width, $height, $charsCount ) 
	{
		// private method will generate code and store it in the variable
		$code = self::generateCode($charsCount);
		
		$width = self::checkWidth($width);
		
		// font size will be 60% 
		$fontSize = $height * 0.6;

		// image creation
		$image = imagecreate($width, $height) or die('Cold not create new image.');
		
		// set up background color
		$background_color = imagecolorallocate($image, 204, 255, 153);

		// set up text color
		$text_color = imagecolorallocate($image, 0, 102, 0);

		// set up noise color
		$noise_color = imagecolorallocate($image, 51, 153, 51);

		// generate dash on the background
		for ($i = 0; $i < ($width * $height) / 3; $i++) {

			imagefilledellipse($image, mt_rand(0, $width), mt_rand(0, $height), 1, 1, $noise_color);
		}

		// generate commas on the background
		for ($i = 0; $i < ($width * $height) / 150; $i++) {

			imageline($image, mt_rand(0, $width), mt_rand(0, $height), mt_rand(0, $width), mt_rand(0, $height), $noise_color);
		}

		// create picture's borders and add text 
		$textbox = imagettfbbox($fontSize, 0, self::$font, $code)
			or die('Error in imagettfbbox function');


		// 0 = lower left x-coordinate  
		$x = array();
		
		$paddingLeft = ($width / $charsCount)*0.25;
		
		for ($i = 0; $i < $charsCount; $i++ )
		{
			$x[$i] = ($width / $charsCount) * $i + $paddingLeft + ( $i == 0 ? ($paddingLeft/2) : 0 );
		}
		
		// 1 = lower left y-coordinate
		$y = array();
		$y[0] = ($textbox[1] + 30);
		$y[1] = ($textbox[1] + $height * 0.7);
		$y[2] = ($height * 0.9);
		$y[3] = ($height / 2);
		$y[4] = ( ($height*2.5) / 3);
		$y[5] = ( ($height/2) + $height / 4);

		shuffle($y);

		// angle of text rotation
		shuffle( self::$charsAngels );

		$k = 0;
		foreach (self::$code as $char) {
			// write text into picture
			imagettftext($image, $fontSize * 0.7, self::$charsAngels[$k], $x[$k], $y[$k], $text_color, self::$font, $char)
				or die('Error in imagettftext function');
			$k++;
		}

		// header set up
		header("Content-type: image/png;");
		header('Cache-Control: no-cache, no-store, must-revalidate'); // HTTP 1.1.
		header('Pragma: no-cache'); // HTTP 1.0.
		header('Expires: 0'); // Proxies.

		// image output
		imagepng($image);

		// destroy image
		imagedestroy($image);

		// insert the code into session
		$_SESSION['captcha']['code'] = $code;
	}

	
	/**
	 * Generate letters code
	 * @param string $characters
	 * @return string
	 * @access private
	 * @static
	 */
	private static function generateCode ($characters) 
	{
		// list of letters from which code will be generated
		$possible = '123456789ABCDEFGHJKLMNOPRSTVUXYZ';
		$code = '';
		$i = 0;

		// cycle 
		while ($i < $characters) {
			// randomly pick letter and assign it to private data array "code"
			self::$code[$i] = substr($possible, mt_rand(0, strlen($possible) - 1), 1);

			// assign letter to variable "$code" 
			$code .= self::$code[$i];
			$i++;
		}
		return $code;
	}

}
